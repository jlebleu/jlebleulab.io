+++
title = "Refactoring"
date = 2022-11-02
draft = true

[taxonomies]
categories = ["TDD", "Refactoring"]
tags = ["TDD", "Refactoring"]
+++

Quelques réfexion suite aux scéances hebdomadaires de refactoring que j'anime avec une équipe sur un gros morceau de logiciel legacy (3 ans d'ages !!!)
On fait çà en équipe pendant une heure et on améliore le code petit à petit.

Avant de démarre on s'assure qu'on peut lancer tout les tests de l'application .... et donc on 
- test

- refactor
On recherche le plus petit refactoring possible, et on reste concentré, pas de renomage intempestif, pas de suppression ou remaniement de code ailleurs qu'à l'endroit ou on intervient

- test

<!-- more -->


```scss
$mw:50% !default;// max-width
$mb:1200px !default;// value at which to switch from fluid layout to using max-width
```
