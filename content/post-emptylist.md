+++
title = "Transform a list of errors to a Cats Validated "
date = 2023-05-23
draft = true

[taxonomies]
categories = ["Scala", "ErrorHandling"]
tags = ["Scala", "Cats", "ErrorHandling"]
+++

Problem: we want to trap all errors occurring when calling a list of services, some of them may succeed, others may fail.


```scala

....

final case class ApiError(code: Int, message: String) {
  override def toString = {
    s"[$code] $message"
  }
}

//
// Simple api error https://www.scala-lang.org/api/2.13.6/scala/util/Either.html
// Allows to get a result or an error
//
type ErrorOr[A] = Either[ApiError, A]
//
// Combined Api error,
// Allows to get a result or a list of error 
// ref: https://typelevel.org/cats/datatypes/validated
//

type ErrorsOr[A] = ValidatedNel[ApiError, A]
....

def createClosure(closure: GraaClosure)(implicit cnx: IConfService): ErrorsOr[String] = {
    log.info(s"request to create ${closure.label} for services : ${closure.services}")
    closureService.createClosure(closure) match {
      case Right(statDay) =>
        val a = closure.services
                  .map(service => closureService.addClosureToService(closure, statDay, service))
        // Here a is a List[ErrorOr[String]] and we need 
        val y =  a.partitionMap {
            case Right(str)  => Right(str)
            case Left(error) => Left(error)
          }
        //
        // Now val y: (List[GcceApiError], List[String])
        //
        NonEmptyList.fromList(y._1) match {
          // No error
          case None        => Validated.Valid(s"Closure ${closure.label} created successfully")
          // Almost one error a non empy list :)
          case Some(value) => Validated.Invalid(value)
        }

      case Left(error) => error.invalidNel
    }
  }
```