+++
title = "Les Langages Informatiques"
date = 2023-12-13
draft = true
+++

## Amusant

Les langages informatiques qui aparaissent dans mon radar, malgré la multitude de langages existants cela continue à foisonner
**Unison** is a friendly programming language from the future: statically-typed, functional, and a lot of fun 😄

https://www.unison-lang.org/
**Gleam** is a **friendly** language for building **type-safe** systems that **scale**!
https://gleam.run/