+++
title = "Doc et Pdf sont dans un bateau"
date = 2023-12-12
draft = false

[taxonomies]
categories = ["Development"]
tags = ["Doc", "Development", "Pandoc", "VisualStudio"]
[extra]
keywords = "USK, Urbansketcher, Pixelfed"
toc = true
image = "images/post-pdf-doc.jpg"
+++
{{ img(src="/images/post-pdf-doc.jpg" alt="Qui veut encore de la tarte aux cerises ?") }}
*Non la documentation n'est pas un casse croûte*

## La documentation dans un projet 

Dans les projets auxquels je participe, j'aime bien mettre la documentation dans un répertoire **doc**, sous forme de fichiers *markdown*.\
Dans le **readme.md**, que l'on trouve traditionnellement à la racine du projet, on y met les informations essentielles pour *bootstraper* le projet dans un environnement de développement.
En plus, ce que je fait d'habitude, c'est de mettre un lien dans ce fichier vers la documentation qui se trouve dans le fameux répertoire **doc**. 

L'avantage est que la documentation se retrouve au même endroit que le code, donc facile à mettre à jour et à maintenir. L'inconvénient est qu'il n'est pas facile de la transmettre ou de la publier. 

Suite à une demande d'un de mes clients, je me suis mis à la recherche d'une solution de transformation de ces fichiers vers du pdf.

Il y a principalement trois solutions :

- Des sites spécialisés
- Des extensions Visual Studio Code
- Pandoc

## Les sites spécialisés

Il y en a pléthore, je ne m'étendrai pas sur cette solution, qui sont souvent une occasion de faire de la publicité, de générer des clicks et de récupérer au passage de l'information.

## Les extensions Visual Studio

### Visual Studio Code Markdown PDF

Cette [extension](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf) utilise chromium pour faire la conversion et est assez longue à installer. Elle fonctionne parfaitement pour un fichier, mais ne possède pas (ou je ne l'ai pas trouvé.), de mode multi-fichiers.
Il y a de plus une astuce pour l'utiliser sous wsl (Ah les bonheurs du développement sous windows) je vous la donne ci-dessous.

L'extension nécessite pour fonctionner en *remote wsl* sous visual studio code l'installation des paquets suivants (sous ubuntu) :

```bash
sudo apt-get install libxcursor1
sudo apt install libxss1
sudo apt install libpangocairo-1.0-0
sudo apt install libgtk-3-0
```
Une fois ces paquets installés, la conversion est utilisable et le résultat est plus que correct, mais mono fichier. Il faut donc faire un pdf par fichier, ce qui dans mon cas, car j'ai l'habitude de séparer les fichiers n'est pas la solution idéale.


### Pandoc

[Pandoc](https://pandoc.org/) est une solution *'Universelle de conversion de documents'* comme indiqué sur leur site.\
C'est effectivement ce qui est le plus efficace même si vous allez le voir, cela demande un peu de préparation et de recherche.\
Pandoc est capable de transformer non seulement du markdown, mais aussi par exemple du reStructuredText, de l'AsciiDoc, etc... Tous ces formats peuvent être transformés soit en HTML, en PDF, en EPUB en Docx en wiki markup formats (MedaiWiki, DocuWiki) et beaucoup d'autres formats.
Pandoc est écrit en Haskell, et c'est un utilitaire en ligne de commande.

Vous écrivez un document sans (trop) vous occuper de la mise en page, et Pandoc, avec le paramétrage qui va bien s'occupe du reste.

#### Exemple

```bash
pandoc -s *.md doc/*.md -o projectdoc.pdf --from=markdown+rebase_relative_paths \
  --toc --pdf-engine=xelatex -V mainfont="Ubuntu" --number-sections --metadata-file doc/metadata.yml 
```

- **-s** : Les fichiers à transformer
- **-o** : Le fichier résultat
- **--from=markdown+rebase_relative_paths** : important garde les chemins relatifs de liens et des images
- **--toc** : génère le sommaire, en parcourant la totalité des fichiers
- **--pdf-engine=xelatex** : utilise un processeur pdf qui gère un peu mieux les polices de caractères.
- **-V mainfont="Unbuntu"** : change la police par défaut (je n'aime pas trop visuellement la police utilisée par défaut)
- **--number-sections** : Option *magique* qui numérote les têtes de chapitres, pas besoin de s'en occuper dans le texte
- **--metadata-file** doc/metadata.yml, permet de gérer les options qui sauvent la mise en page sans alourdir la ligne de commande.

Comme je vous le disais précédemment la mise en oeuvre de l'outil n'est pas directe et nécessite pas mal d'essais et pas mal de lecture de documentation et d'articles sur son utilisation, mais le résultat est vraiment pas mal.

#### Le fichier metadata.yml

C'est un fichier de configuration qui permet de paramétrer le formatage du document. Je vous livre le mien

```yml
---
title: Project documentation
header-includes: |
    \usepackage{geometry}
      \geometry{
        a4paper,
        left=15mm,
        top=20mm,
        right=15mm
      }
    \usepackage{fancyhdr}
      \pagestyle{fancy}
      \fancyhead[L]{\date{\today}}
      \fancyhead[C]{MYProject}
      \fancyhead[R]{\thepage}
    \usepackage{fvextra}
       \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}

```

Les options sont assez faciles à comprendre, et sont des options pour le processeur LaTex, a noter tout de même les deux dernières lignes qui permettent de gérer les retours à la ligne dans les blocs de code.

Si vous avez de la doc à écrire, je vous conseille vraiment de vous pencher sur cet outil. Il y a beaucoup d'autres possibilités, qui lorsque l'on écrit de gros documents sont particulièrement utiles, comme par exemple la gestion des citations de la bibliographie.

### L'installation

Sous ubuntu, le paquet de la distribution est trop ancien, je vous conseille donc de télécharger la [dernière version ici](https://github.com/jgm/pandoc/releases/). Une fois le paquet télécharger l'installer  `sudo dpkg -i pandoc-3.1.9-1-amd64.deb`.\

Puis installer les paquets suivants:

```bash
 sudo apt install pdflatex
 sudo apt install texlive-latex-base
 sudo apt install texlive-latex-recommended
 sudo apt install texlive-xetex
 sudo apt install texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra

```
## Références

Quelques sites qui m'ont aidés 

- Le site officiel de [Pandoc](https://pandoc.org/)
- [Redaction durable avec Pandoc et Markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown#utilisation-de-pandoc-pour-convertir-le-markdown-en-document-ms-word) du site [Programming Historian](https://programminghistorian.org/fr/)




