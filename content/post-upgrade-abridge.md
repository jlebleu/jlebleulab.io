+++
title = "Mise à jour du thème Abridge"
date = 2023-11-09
draft = false

[taxonomies]
categories = ["Infra"]
tags = ["Abridge", "GetZola", "Infra"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Infra"
+++

Je viens de mettre à jour non sans difficultées le thème [abridge](https://github.com/Jieiku/abridge) que j'utilise sur ce blog avec [getzola](https://www.getzola.org/).
Reste à bidouiller un peu le css pour la taille des fonts, et gérer correctement le multilangue.

Voici mes notes (*en anglais*):

- Remove content (except posts)
- Remove template
- Save config.toml

```bash
git submodule update --remote
```

Follow doc

```bash
 touch templates/.gitkeep
 rsync themes/abridge/config.toml config.toml
 rsync themes/abridge/content/_index.md content/
 rsync themes/abridge/COPY-TO-ROOT-SASS/* sass/
 rsync themes/abridge/netlify.toml netlify.toml
 rsync themes/abridge/package_abridge.js package_abridge.js
 rsync themes/abridge/package.json package.json
 rsync -r themes/abridge/content .
```

Update config.toml and saas/abridge.css (social icons)

