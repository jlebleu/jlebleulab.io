+++
title = "Tout sur les Aberscketchers"
date = 2023-11-12
draft = false

[taxonomies]
categories = ["Dessin"]
tags = ["usk", "uskfrance", "urbansketchers", "abersketchers", "croquis"]
[extra]
keywords = "abersketchers, usk, croquis"
toc = true
series = "croquis"
image="/images/IMG_20220815_131107.jpg"
+++

{{ img(src="/images/IMG_20220815_131107.jpg" alt="Vue des carnets lors d'une rencontre") }}


## Histoire

Le groupe *Aberscketchers* en référence aux [Urbanscketchers](https://france.urbansketchers.org/) s'est réunis pour la première fois au [Bzztcafé](https://www.instagram.com/bzzzt.cafe/?hl=fr) à Plouguerneau le 12 Juin 2022. 
Depuis nous nous rencontrons de loin en loin souvent le dimanche matin de 10h30 à 12h30 environ, et nous prolongeons la rencontre, si le temps le permet d'un pique nique partagé.
Les sorties sont pour l'essentiel autour de Landéda, Plouguerneau et Lannilis

Depuis cette date, nous nous sommes réunis une bonne quinzaine de fois.

## Organisation

Les réunions se passent de préférence à l’extérieur nous sommes néanmoins obligés de temps en temps de nous retrouver à l'intérieur surtout l'hiver.

Il n'y a pas réellement d'organisation, chacun peut convoquer une réunion à son gré, il suffit d'avoir envie de dessiner quelque part et de partager son rendez vous sur la liste de diffusion du groupe.
Pour être au courant des informations du groupe, on s'inscrit à la liste de diffusion du groupe [Aberscketchers](https://framagroupes.org/sympa/info/abersketchers) liste hébergée par [framagroupes](https://framagroupes.org/). 
En général, quelqu'un m'envoie un mail et j'ajoute la personne à la liste de diffusion.

## Les rencontres

Les séances de dessin sont collectives, sans animateur, chacun amène son matériel et dessine ce qui lui fait le plus envie sur le moment.
Il n'y a pas d'obligation d'utiliser tel ou tel medium, et toutes les techniques sont les bienvenues, y compris les techniques numériques.

A la fin du rendez-vous nous partageons nos dessins et publions [les photos ici dans un répertoire partagé](https://kdrive.infomaniak.com/app/share/627989/67f65665-a1df-4d28-b8fa-391b679f248d)

## Vers un événement l'année prochaine

Nous prévoyons un week-end d'échange autour du dessin le week-end du 25 et 26 mai que nous organiserions conjointement avec l'atelier d'aquarelle (à confirmer)

### Quelques idées pour le week-end

- Exposition des participants
- Journée *parcours de croquis* avec points de rendez-vous, pique nique partagé le midi et retrouvailles en salle pour mise en commun des oeuvres et apéro puis repas commun le soir pour les inscrits
- Invitation des groupes proches (Morlaix, Brest, Quimper, Lorient)
- Ateliers de démarrage pour ceux qui veulent ce lancer
- Autres ateliers pour ceux qui veulent échanger leurs savoirs.
- Et beaucoup d'autres idées .... j'espère.

## Fonctionnement avec Echanges et Savoir.

Nous sommes en discussion avec l'association [*échanges et savoir*](https://www.echanges-et-savoirs-landeda.fr/) de Landéda, avec qui nous aurions des intérêts communs le mouvement des **urbansketchers** étant aussi dans une dynamique de partage.

**Les quelques idées que nous avons évoquées ensemble lors de notre dernière réunion :**

- Nous sommes à la recherche de salle surtout l'hiver, environ 2 fois par mois.
  - Nous pouvons convenir à l'avance de dates et de créneaux horaires pour cette période.
- Il est bien entendu que pour profiter des séances en salle lors des rencontres dans le cadre d'échange et savoir, les participants devront être à jour de la cotisation.
- Par contre en dehors des séances en salle *échanges et savoirs* l'accès aux rencontres reste libre et sans obligation de cotisation.
- Le nom du groupe reste **Aberscketchers**
- On continue à utiliser la liste de diffusion pour organiser nos rendez vous.


## Quelques références.

Le mouvement Urbansketchers : [https://urbansketchers.org/fr/](https://urbansketchers.org/fr/)

Le mouvement Français : [https://france.urbansketchers.org/](https://france.urbansketchers.org/)

### Vision et valeurs : Le manifeste des Urbansketchers

- Nous dessinons sur place, à l'intérieur ou à l'extérieur, en capturant ce que nous voyons par observation directe.
- Nos dessins racontent l'histoire de notre environnement, de nos voyages et des lieux où nous vivons.
- Nos dessins sont des enregistrements du temps et du lieu.
- Nous sommes fidèles aux scènes dont nous sommes témoins.
- Nous utilisons tout type de support et valorisons nos styles individuels.
- Nous nous soutenons mutuellement et dessinons ensemble.
- Nous partageons nos dessins en ligne.
- Nous montrons au monde, un dessin à la fois.