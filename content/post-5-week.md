+++
title = "Cinquième Semaine"
date = 2023-12-06
draft = false

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/post-5-week.jpg"
+++
{{ img(src="/images/post-5-week.jpg" alt="L'éternité des vagues") }}
*L'éternité des vagues*
___

**ARNOLPHE**

*"Quelle nouvelle ?"*

**AGNÈS**

*"Le petit chat est mort."*

**ARNOLPHE**

*"C’est dommage : mais quoi
Nous sommes tous mortels, et chacun est pour soi.
Lorsque j’étais aux champs n’a-t-il point fait de pluie ?"*

**AGNÈS**

*"Non."*

___

Bon voyage au paradis des chats, après ce long voyage qui l'a menée des terres froides du Canada à notre beau pays de Bretagne, venteux, quelques fois humide, mais si lumineux.
Que ces quelques fleurs de printemps que j'ai semé pour l'occasion t'accompagne ainsi que les fleurs de sauge et le saule que nous avons laissé pour toi.

J'expérimente en ce moment un portage d'une application utilisant play framework de [Scala](https://www.scala-lang.org/) en [Java](https://www.java.com/fr/). Je ne m’étendrai pas sur les raisons qui ont poussé à faire cela, bien qu'on puisse légitimement se demander ce qui pousse les développeurs à ne pas sortir du Java pour essayer le Scala. Autant les démarrages sont assez facile, car on peut faire au début du java-scala, mais au fur et à mesure que l'on progresse, le fonctionnel prends le dessus et le code écrit est souvent relativement difficile à lire pour ceux qui ne sont pas familiers avec la tournure fonctionnelle. Cela mériterait, je pense un peu plus de développement, mais ce sera pour une autre fois.
Pour le portage en Java je reste sur du [Play Framework](https://www.playframework.com/) version Java, un petit upgrade quand même du JDK et un passage vers des acteurs typés. L'expérience est intéressante et me pousse à trouver des solutions fonctionnelles en Java, comme par exemple l'`Optional<T>`. Première étape l'authentification en cours, la connexion LDAP est opérationnelle et je tente la suite qui est la mise en place d'un acteur qui gère la connexion vers un serveur de configuration Genesys. Affaire à suivre. Cela fait bien longtemps que je n'avais pas touché à ce langage, et cela fait du bien.


J'ai travaillé cette semaine sur deux aquarelles que j'ai postées sur mon compte [Pixelfed](https://pixelfed.fr/jlebleu). Par la même occasion, j'ai craqué pour la série d'[Humble bundle](https://www.humblebundle.com/books/how-to-start-drawing-with-walter-foster-books) *How to Start Drawing* et j'ai aimé plus particulièrement dans cette série le livre de Veronica Lawlor [One Watercolor a Day](https://www.fnac.com/mp27824573/One-Watercolor-a-Day-Veronica-Lawlor) qui rejoint à peu près ma conception de l'aquarelle. De l'eau de l'eau de l'eau.


J'ai lu cette semaine une belle bande dessinée d'[Etienne Davodeau](https://www.futuropolis.fr/auteurs/etienne-davodeau.html) le [Droit du sol](https://www.futuropolis.fr/9782754829212/le-droit-du-sol.html) qui m'a fait réfléchir sur le facteur temps. A voir aussi [le site d'Etienne](https://www.etiennedavodeau.com/).

L'auteur part de la grotte de [Pech Merle](https://www.pechmerle.com/) et, à pied, trace son chemin jusqu'à Bure, là ou nous projetons d'enfouir nos déchets nucléaires. Au-delà de la problématique du nucléaire et de ses déchets, se pose la question du temps. Les dessins des grottes préhistoriques, sont à peu près tout ce qui nous reste de cette époque, soit il y a à peu près 25 000 à 35 000 ans. Comment peut-on imaginer que dans 100 000 ans, oui vous avez bien lu cent mille ans, il y aura encore des traces utilisables des procédures et des précautions que nous devons suivre pour ce stockage hautement toxique? Dans un des entretiens racontés dans le livre, il évoque la pérennité des différents supports comme par exemple le papier, de l'écriture et la communication, des langues.
J'ai beaucoup aimé son trajet, l'histoire qu'il nous raconte, les rencontres ainsi que les entretiens qu'il met en scène tout au long du voyage avec des personnages bien réels, mais qui l'accompagne sur la route en imagination.
Merci pour ce livre. Ca donne envie de partir à pied.


Que les prochains jours vous soient propices et à la semaine prochaine.
