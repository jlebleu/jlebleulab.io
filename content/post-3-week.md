+++
title = "Troisième Semaine"
date = 2023-11-22
draft = false

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/post-3-week.jpg"
+++

{{ img(src="/images/post-3-week.jpg" alt="Le meilleur véhicule du monde") }}
*Le meilleur véhicule du monde*

*7h44*

*C'est parti, je garde mon rendez-vous hebdomadaire, et qui plus est un mercredi (c'était bien mercredi je crois la [semaine dernière](@/post-2nd-week.md) non ?). Cette fois ci je ne suis pas dans le train à 300 km/h mais assis à mon bureau à 1670 km/h, si on prends en compte la vitesse de la rotation de la terre :)
Je continue le défi d'écrire au moins une fois par semaine, et donc je vais devoir certainement écrire deux fois car l'idée était de publier au moins un article technique par semaine, celui-ci restant un article plutôt général, presque le ["en vrac" de Tristant Nitot](https://www.standblog.org/blog/) que j'aime beaucoup. Cela fait un moment que je ne l'ai pas lu d'ailleurs.*

Une semaine un peu chargée, qui tourne toujours autour des trois activités que j'aie en ce moment, le dessin avec les [Aberscketchers](@/post-aberscketchers.md), les [système d'information géographiques](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27information_g%C3%A9ographique) (GIS) et le development d'une application en scala utilisant [Play Framework](https://www.playframework.com/) pour piloter un centre de contact [Genesys](https://www.genesys.com/) ainsi que l'écriture d'un post sur la [revue de code](@/post-revue-de-code.md).

## Gis

Après avoir passé pas mal de temps à écrire un rôle [Ansible](https://www.ansible.com/) pour déployer la stack [Lizmap](https://www.lizmap.com/) avec docker-compose sur une machine de chez [Hetzner](https://www.hetzner.com/), je me suis lancé dans l'étude du code de l'affichage de cartes afin de pouvoir réaliser une intégration sans passer par [Qgis](https://www.qgis.org/) desktop pour créer le projet. La génération des fichiers projets Qgis et Lizmap a été assez facile (merci Arnaud), par contre pouvoir lancer l'affichage web sans configuration préalable nécessite de descendre dans le code du client web de Lizmap. Pour l'instant, malgré la piste sérieuse que j'avais de créer des *repositories* à la volée, ce n'est pas concluant. Travail en cours donc.

A noter sur le front de la géographie [Openlayers](https://openlayers.org/) bibliothèque d'affichage et d'intégration de cartes et le futur site [gouvernemental des cartes et données du territoire](https://cartes.gouv.fr/).

## Dev

Sur le développement en [Scala](https://www.scala-lang.org/) et Play, j'avais omis au départ de tester les contrôleurs, en effet cela est souvent très consommateur de temps car on doit monter la stack web complète avant de lancer les tests, j'ai trouvé un compromis en les testant unitairement et en écrivant des helpers de tests qui simplifient grandement l'écriture des tests, malgré l'utilisation d'acteurs Akka qui permettent l'interconnexion avec d'autres serveurs. 

J'avais mis en place une architecture à base de contrôleurs / *backcontrollers*, ou l'essentiel du travail est fait dans le *backcontroller* qui lui est complètement testable car hors de la stack web. Grace à cette opération, je démarre désormais les développement des nouvelles routes en TDD !
La nouvelle version de Play m'a permis aussi de remettre en place le rapport de couverture de tests, malheureusement que 70%.

## Dessin

Une petite séance de dessin avec les [Aberscketchers](@/post-aberscketchers.md) salle Nuit de Noce dimanche dernier. J'ai aussi pris part à un premier atelier avec [Jean-Marc Jezequel](https://jeanmarcjezequel.fr/ansib) à Guisseny ou j'ai commencé à peaufiner mon usage de l'aquarelle.

J'ai lu cette semaine "Nemesis" d'[Isaac Asimov](https://fr.wikipedia.org/wiki/Isaac_Asimov), que je n'avais pas encore lu, moi qui pensait avoir tout lu de lui. Je suis toujours fan de ses personnages, des dialogues et de cette histoire de l'humanité. Ici on est au prémisse de l'essaimage des hommes dans la galaxie, avec dans les personnages principaux une jeune fille qui m'a fait penser à la petite fille de la maire de Terminus dans la série "Fondation". Pas de robots par contre dans ce roman. Toujours un plaisir à lire.

A noter aussi le visionnage d'une [video](https://vimeo.com/879294549) d'Adrian Cockcroft de sur laquelle je reviendrai et l'écoute d'un podcast d'[IFThenDev](https://www.ifttd.io/).

Ce sera tout pour cette semaine, il va me falloir du temps je pense pour améliorer mon écriture, car c'est encore un peu laborieux. 

Portez vous bien et à la semaine prochaine j'espère.

*08h03-08h36*