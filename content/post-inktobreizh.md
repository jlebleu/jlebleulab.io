+++
title = "Inktobreizh 2023"
date = 2023-10-12
draft = false

[taxonomies]
categories = ["Dessin", "Inktober", "Inktobreizh"]
tags = ["Pixelfed", "Inktober", "Inktobreizh"]
[extra]
keywords = "Pixelfed, Inktober, Inktobreizh"
toc = true
series = "Urbansketcher"
+++

Cette année je retente le challenge dessin du mois d'octobre

Le defi [inktober](https://fr.wikipedia.org/wiki/Inktober) 2023 et sa [version bretonne](https://www.letelegramme.fr/finistere/brest-29200/les-brestois-deor-glas-studio-relancent-leur-challenge-creatif-doctobre-sur-instagram-6434780.php)


J'ai choisi la version bretonne avec sa liste de mots 
![liste de mots](https://pixelfed.fr/storage/m/_v2/565892503158459726/f1538e3aa-7b3151/5gGfKywVF46s/XlMYGtEQdrcDhk0amm9lzCKkgGgBb64qhjZf4wax.jpg)

11 jours de dessins à l'encre, un exercice à tenter, pas facile de dessiner à l'encre directement et de simuler les couleurs.

Je suis un peu seul sur le tag [breizhtober2023](https://pixelfed.fr/i/web/hashtag/inktobreizh2023) sur [pixelfed](https://pixelfed.fr/i/web) 
par contre pas mal de monde qui s'essayent à [inktober2023](https://pixelfed.fr/i/web/hashtag/inktober2023)


Allez le combat continue ... bon dessins à tous.