+++
title = "Pixelfed, nous voilà !!!"
date = 2023-05-22
draft = false

[taxonomies]
categories = ["Dessin"]
tags = ["USK", "Urbansketcher", "Pixelfed", "Fediverse","Abersketchers"]
[extra]
keywords = "USK, Urbansketcher, Pixelfed"
toc = true
series = "Pixelfed"
image = "images/pixelfed.jpg"
+++

Allez aujourd'hui je démarre sur [PixelFed](https://pixelfed.fr/i/web/profile/565892503158459726) 

Après un choix rapide du serveur sur <https://pixelfed.org/servers>, pas très compliqué je prends l'[instance française pixelfed.fr](https://pixelfed.fr) et hop me voila parti !!!

Un premier post sur notre dernière rencontre d'*Urbansketcher*, à la [chapelle de Tromenec](https://abers-patrimoine.bzh/kpoi/chapelle-saint-laurent-a-tromenec/) de Landéda avec les *Aberscketchers*.

De belles contributions des participants
 ![contributions](https://pixelfed.fr/storage/m/_v2/565892503158459726/c537ce87c-f5971d/PZmopjkeIUki/xfdAdmxg8fzPwmiubIqrcNTAC1hZKovNrgPBz3BC.jpg)

Adieu Instagram
