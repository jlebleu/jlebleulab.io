+++
title = "Un article par semaine"
date = 2023-11-08
draft = false

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/carnetmoulinenfer.jpg"
+++

*08h16*
{{ img(src="/images/carnetmoulinenfer.jpg" alt="Rencontre au moulin de l'enfer") }}

C'est parti aujourd'hui je prends une décision. Après avoir tourné longtemps autour du pot et lu quelques articles ici ou la sur la pratique régulière de l'écriture, je me lance....
Un article par semaine.
Il est difficile de ne pas se laisser distraire, je saute donc sur mon [éditeur de code préféré](https://code.visualstudio.com/), je refrène un peu mon envie de trouver comment mettre en place 
un correcteur orthographique en français (vous voyez les fotes ?) et en avant.

Mon dernier article date d'y il y a un mois, sur le defi [inktober](https://fr.wikipedia.org/wiki/Inktober) que j'ai réussi à suivre, un dessin par jour, j'en ai posté quelque uns sur [pixelfed](https://pixelfed.fr/i/web/profile/565892503158459726) par contre je ne les ai pas tous postés.

## Un livre à lire 

Vais je acheter le livre de [Johanna Rothman](https://framapiaf.org/@johannarothman@mastodon.sdf.org) sur leanpub [Free Your Inner Nonfiction Writer](https://leanpub.com/freeyourinnerwriter) ? Cela me permetterait peut-être de lancer cette fois un blog technique et de m'y tenir.

J'ai pas mal de sujets en ce moment autour d'[Ansible](https://www.ansible.com/), de [Lizmap](https://www.3liz.com/opensource.html) que je suis en train d'explorer et d'un projet en [Scala](https://fr.wikipedia.org/wiki/Scala_(langage)) utilisant [Play Framework](https://www.playframework.com/) mais ca ce sera pour le prochain article.

En tout ca je continue ma prise de notes avec [Obsidian](https://obsidian.md/) qui sera j'espère une source d'inspiration

Allez le combat continue ... bon écriture à tous et à la semaine prochaine.

*Ps: C'est bon je rajoute quelques liens, une p'tite photo en haut de l'article et je publie (git commit, push et [gitlab](https://gitlab.com) va faire le reste)*

*08h33*
