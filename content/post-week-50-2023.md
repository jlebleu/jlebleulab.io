+++
title = "Sixième Semaine"
date = 2023-12-13
draft = false
description = "Billet hebdomadaire, écriture, projets, dessin et lectures."

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog", "QGIS", "Dev"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/post-week-50-2023.jpg"
+++
{{ img(src="/images/post-week-50-2023.jpg" alt="De la lumière dans l'hiver") }}
*De la lumière dans l'hiver*


Sixième billet hebdomadaire, je tiens le rythme depuis la promesse que je me suis faite le [8 Novembre 2023](@/post-weekly.md). Ce sera donc le dixième billet publié depuis cette date, soit un peu plus d'un billet par semaine. Il est encore trop tôt je pense pour dire si ma capacité d'écriture s'est améliorée, car une séance par semaine me paraît trop peu pour que cela fasse un entraînement efficace.\
Il n'empêche, le premier billet faisait un peu moins de 300 mots, maintenant j'écris plutôt un peu plus de 600 mots sans trop de problème ce qui fait une progression certaine. \
Une fois n'est pas coutume, j'ai noté cette semaine dans mon obsidian préféré, ce qui m'a marqué et je vous en ferai part à la fin de cette article.

## L'écriture

Je reviens un peu sur mon *process* d'écriture. Je fais d'abord un premier jet à la volée, sans (trop) de corrections afin de ne pas perdre le fil de mes idées, et c'est efficace, car dés que je reviens en arrière le fil est coupé et le redémarrage est vraiment difficile, même si c'est compliqué de s'astreindre à laisser passer les fautes, les mal dits. Dans cette phase lorsque je ne suis pas sur d'une information ou d'un lien je la remplace par une série de XXX sur laquelle je reviendrai ensuite. 
Une fois l'article terminé, je fais une première relecture, je corrige principalement les fautes les plus évidentes quelques tournures de phrases. Je commence ensuite la mise en forme, j'ajoute les liens là ou c'est pertinent. Je reprends ensuite le texte et je le copie colle dans un correcteur d'orthographe en ligne qui me donne encore d'autres améliorations possible et des fautes qui m'ont encore échappées (Ah l'orthographe !). Pour terminer l'article, je choisis une photo dans ma galerie, et je l'ajoute à l'article avec un texte alternatif et un commentaire.
Le travail est fait, il ne reste plus qu'à le publier en le *'poussant'* sur Gitlab, le CI faisant le reste c'est à dire la mise en ligne. A lire aussi sur le sujet le blog de Thierry Crouzet, [Edition la fin des correcteurs ?](https://tcrouzet.com/2023/12/01/edition-la-fin-des-correcteurs/)

## Projets de la semaine

Pas beaucoup de Scala cette semaine, j'ai terminé mon exploration Scala vers Java, en écrivant les parties les plus compliquées. Il reste maintenant la longue traversée du désert à faire, c'est à dire la réécriture des parties métiers et transformations de données. Java étant un langage pas mal plus verbeux que Scala, cela risque d'être un chemin assez ennuyeux. Maintenant par contre j'ai une assez bonne visibilité sur le travail qu'il reste à réaliser. Une semaine de documentation aussi ou j'ai découvert [pandoc](@/post-pdf-doc.md). Le fait de documenter l'architecture du code m'a aussi permis de me poser des questions sur les chemins que j'ai choisi et m'a donné quelques pistes de remaniement de code assez profond. C'est donc un exercice que je vous recommande et qui permet de prendre un peu de recul sur ce que l'on a écrit. Au bout d'un an d'écriture, l'architecture mise en place au départ et qui paraissait pertinente, mérite à être questionnée.

Je continue aussi d'approfondir mes connaissances sur le monde de la [géomatique](https://fr.wikipedia.org/wiki/G%C3%A9omatique) au travers d'un projet de dessin géolocalisé au dessus d'images géo-référencées et de modèles numérique de terrain. J'ai passé de bons moments de collaboration avec la société [3liz](https://www.3liz.com/) éditeur entre autre de [Lizmap](https://www.lizmap.com/). Une semaine découverte des possibilités étonnantes de [PostGis](https://postgis.net/) de [QGIS](https://www.qgis.org/fr/site/) et de Lizmap qui mériterait je pense un petit article tellement il est facile de publier des cartes en exploitant les données ouvertes disponibles maintenant pour le public. Bravo en tout cas pour ce travail en Open Source\
A noter d'ailleurs entre autre de plus en plus de données mise à disposition comme par exemple [la SNCF](https://ressources.data.sncf.com/pages/accueil/), ou le project [Plan de Corps de Rue Simplifié](https://geoservices.ign.fr/pcrs) ou encore la [Plateforme ouverte des données publiques françaises](https://www.data.gouv.fr/fr/).

On ne passe pas une semaine sans crayons et sans pinceaux, je poursuite ma découverte de l'aquarelle avec Jean-Marc Jezequel, à voir ici sur mon compte [Pixelfed](https://pixelfed.fr/i/web/post/639945754309525349).

## Lectures

Je poursuis la lecture, sans trop d'enthousiasme d'un roman policier, par contre de belles lectures sur le web.\ 

- Un article de Ploum, qui date un peu sur [le silence au milieu du bruit](https://ploum.net/le-silence-au-milieu-du-bruit/index.html) qui réfléchit sur la masse d'information qui nous assaille en permanence et surtout sur l'utilité et la qualité de cette information. Est-il nécessaire d'en rajouter, et pourquoi prendre la peine de rédiger ce blog en devenir ?
- [Dévendre la croissance](https://timotheeparrique.com/reponse-a-bruno-le-maire-devendre-la-croissance/) La décroissance n’est pas plus source d’appauvrissement que la croissance est source d’enrichissement de Thimothée Parrique
- [Bye bye WhatsApp, hello ?](https://mnt.io/2021/01/19/bye-bye-whatsapp-hello/) A garder sous le coude pour alimenter les discussions que vous ne manquerez pas d'avoir pendant les fêtes autour de Whatsapp, Signal, Matrix, Telegram etc de Ivan Enderlin.


## J'ai lu en Vrac

- Une [alliance entre quelques grands groupes](https://news.itsfoss.com/ai-alliance/) autour de l'intelligence artificielle qui devrait empêcher une seule entreprise à dominer cette technologie. C'est un sujet qui me préoccupe de plus en plus.
- [Lynis](https://cisofy.com/lynis) Une suite d'outils de consolidation des systèmes d'exploitation Linux au niveau de la sécurité en GPL. C'est aussi un sujet d'actualité dans mes pratiques quotidiennes.
- On ne vous a jamais appris à écrire des logiciels de qualités par Florian Bellman. Quelques citations ci dessous qui résonnent en moi.
  > *"deliver software without bugs in time"* \
  > *"We need to speak about money. As developers, we need to speak about the cost of not doing QA. This is the language of business and managers in general"*\
  > *"I like the term 'minimal effective dose' (MED). The smallest dose that will produce the desired outcome"*\

## A retenir

Framasoft [mobilizons nous](https://framacolibri.org/t/mobilizon-nous/19752), cherche du monde pour continuer les développements de [Mobilizon](https://joinmobilizon.org/fr/) un outil qui permet de trouver, créer et organiser des événements.


Belle semaine à tous.