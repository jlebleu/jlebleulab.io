+++
title = "Deuxième semaine le retour"
date = 2023-11-15
draft = false

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/ilevierge.jpg"
+++

{{ img(src="/images/ilevierge.jpg" alt="L'ile vierge vue de Stagadon") }}
*L'ile vierge vue de Stagadon*

*Commencé à 17h12*

Chose promise, chose due, je reviens pour la deuxième semaine. Comme je l'ai dit dans ce [précédent article](@/post-weekly.md), voici le deuxième post de la série *un article par semaine*.

J'ai donc craqué et acheté le livre de [Johanna Rothman](https://www.jrothman.com/) pour [libérer l'écrivain qui est en moi](https://www.jrothman.com/books/free-your-inner-nonfiction-writer/) et me voila parti.

## Le livre

Quelques mots sur son livre que je viens de terminer.

- **Ecrire ecrire ecrire**

Le secret de l'écriture vous n'en douterez pas c'est d'écrire, plus on écrit plus on sait écrire. 
C'est peut être enfoncer un porte ouverte, mais c'est un conseil que l'on peut appliquer à beaucoup de domaines, l'écriture, le dessin, le développement, le TDD etc....

- **Ecrire pour ses lecteurs**

Il s'agit de se mettre à la place de son lectorat. Ca ne vous rappelle rien ?
Se mettre à la place de l'utilisateur, je raconterai certainement l’anecdote une autre fois.

- **Terminer ce que l'on commence**

Terminer c'est finir l'écriture et surtout *publier*. 
Pour paraphraser quelqu'un que je connais bien, ce qui n'est pas en production n'existe pas .....

- **Ecrire à la volée**

C'est à dire ne pas revenir en arrière pour corriger le text ou le reformuler. 
On démarre par une session de 15 minutes, un peu long pour moi je trouve, et à la fin de chaque session on cycle, c'est à dire on reviens sur ce qu'on a écrit et surtout on n'édite pas. 
La j'avoue que j'ai un peu de mal à comprendre ce que veux dire ce cycle, j'y reviendrai sûrement après y avoir réfléchi.

- **La structure**

On commence par une phrase d'accroche: quel est le problème que l'on essaie d'adresser.  
Ensuite on développe et en conclusion on reviens sur la définition du problème. (j'écris un test j'écris un code et je remanie le code *Refactor*)

Il y a encore beaucoup d'autre choses dans ce livre qui mérite d'être digéré et relu, j'en reparlerai sûrement.

## Pendant la semaine

J'ai tout de même réussi pendant la semaine qui vient de s'écouler à publier deux articles, un sur la [construction de ce blog](@/post-upgrade-abridge.md) et un autre sur les [**abersketchers**](@/post-aberscketchers.md) un groupe de dessin que j'ai créé, une de mes passions avec le développement, et auquel je participe ici à Landéda.

J'ai aussi passé pas mal de temps sur la structure technique de ce blog, pas vraiment satisfait par la mise en page du [thème abridge](https://github.com/jieiku/abridge/), j'en ai essayé un autre [deepthought](https://www.getzola.org/themes/deepthought/). Mais intégrer un autre thème m'a envoyé un peu trop loin alors je me suis mis à modifier le thème abridge en réécrivant une partie des templates et des feuilles de style.

J'ai aussi mis en pratique les conseils de [Johanna Rothman](https://www.jrothman.com/)  en faisant l'exercice d'écrire pendant un temps déterminé, 10 mn en l’occurrence. 
Par contre je ne suis pas allé plus loin pour l'instant, il me reste à réviser l'article et à le publier.

J'oubliais j'ai aussi fait le tour de mes autre blogs démarrés et abandonnés et je me suis apercu que j'avais déjà écrit pas mal de choses !!!

J'ai commencé à semer mes *pierres des champs* fieldstones(voir [Weinberg’s _Weinberg on Writing](https://www.fnac.com/a4970873/Consulting-Secrets-Weinberg-on-Writing-The-Fieldstone-Method-Gerald-M-Weinberg)) et à créer une banque d'idée que je met à jour avec [Obsidian](https://obsidian.md) *(tiens le thème d'un ou deux prochains articles)* 

Comme vous voyez, les idées ne manquent pas il ne me reste plus qu'à revoir un peu la structure de ce post y ajouter quelques liens, une belle image et le publier.

A bientôt.

*Terminé à 17h27 - 18h06*