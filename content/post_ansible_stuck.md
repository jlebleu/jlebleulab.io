+++
title = "Un role Ansible qui coince"
date = 2022-11-03
draft = false

[taxonomies]
categories = ["Ansible", "DevOps"]
tags = ["Ansible"]
[extra]
keywords = "Ansible, Role"
toc = true
series = "Ansible"
image="/images/Demarreur.jpg"
+++

{{ img(src="/images/Demarreur.jpg" alt="Photo de demarreur") }}

Je me suis retrouvé coincé aujourd'hui avec un role ansible qui effectue un déploiement avec git, le repo était bien copié sur le serveur mais ansible ne rendait pas la main après avoir terminé le clone du repo git.
<!-- more -->
J'ai passé pas mal de temps autour de problème avant de m'apercevoir que le repo git contenait maintenant un .gitmodule vers un projet [gitlab](https://gitlab.com/) pour lequel je n'avait pas de token de déploiement.

```yaml
- name: Clone project {{ app_git_repo }} to {{ app_instance_root }}
  git:
    repo: "{{ app_git_repo }}"
    force: true
    dest: "{{ app_instance_root }}"
    version: "{{ app_version }}"
    recursive: false

  become_user: "{{ app_user }}"
  vars:
    ansible_ssh_pipelining: true
```
Du coup j'ajoute un petit force et hop tout rentre dans l'ordre.

Ce qui m'a permis quand même d'explorer le mode debug d'ansible de de m'apercevoir que le module attendait un prompt username, ce qui est impossible à voir même en passant ansible-playbook en mode verbeux -vvvv

## Ansible et le mode debug

Un role qui coince ou qui ne fait pas ce qu'il veux

-vvvv sur la ligne de commande ansible-playbook 

Plus verbeux :

```bash
export ANSIBLE_LOG_PATH=~/ansible.log
export ANSIBLE_DEBUG=True
```
Voir l'article dans la [documentation d'ansible](https://docs.ansible.com/ansible/latest/network/user_guide/network_debug_troubleshooting.html#enabling-networking-logging-and-how-to-read-the-logfile)
