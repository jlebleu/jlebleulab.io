+++
title = "Play Framework"
date = 2022-11-03
draft = true

[taxonomies]
categories = ["Play", "Scala"]
tags = ["Play", "Scala", "Angular"]
+++


Je viens d'avoir l'oportunité de redémarrer un nouveau projet dans un écosystème java devant servir une API en *REST*, et j'ai choisi tout naturellement [Play Framework](https://www.playframework.com) en [Scala](https://www.scala-lang.org/).

<!-- more -->



```scss
$mw:50% !default;// max-width
$mb:1200px !default;// value at which to switch from fluid layout to using max-width
```
