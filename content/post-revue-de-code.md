+++
title = "La revue de code"
date = 2023-11-21
draft = false

[taxonomies]
categories = ["Development"]
tags = ["Development", "Codereview", "Agile"]
[extra]
keywords = "Codereview, Pratiques"
toc = true
series = "Development"
image="images/code_review.jpg"
+++
{{ img(src="/images/code_review.jpg" alt="A la terrasse de l'escale") }}
*Le Bel Espoir et le Rara Avis au mouillage à l'Aberwrac'h*

*Tout le monde a lu ou lit aujourd'hui des articles sur la revue de code (code review en anglais), c'est une pratique qui devient largement répandue. 
Peut-être la pratiquez vous vous même.
Mais se demande t'on si cela est une pratique efficace et surtout à quoi ca sert ?
Qu'est ce que cette pratique peux nous apporter et surtout quel problème peut-elle nous aider à résoudre.*

## Les objectifs

Quels sont les objectifs de la revue de code ?

### L'apprentissage

On ne le répétera jamais assez une équipe qui va vite est une équipe qui apprend et qui est capable d'apprendre. C'est un des facteurs les plus importants de la performance à long terme.
Un des objectifs numéro un de la revue de code est donc l'apprentissage, cela peut se traduire de trois façons différentes :

- La base de code
- Le language utilisée
- L'architecture du code

### La base de code

C'est un moment d'échange entre les participants qui permet, lorsque le volume de code devient relativement important, de suggérer d'autre façon d'implémenter la fonctionnalité en utilisant du code existant. Les développeurs ayant la connaissance la plus approfondie peuvent donner à celui qui est l'auteur du code des pistes de remaniement de code en utilisant des méthodes ou fonctions du code déjà existant.

### Le language

Certains languages ont de subtilités qui méritent à être connues. Je pense par exemple à l'utilisation de modes fonctionnel plutôt que de l'objet en scala, ou encore utiliser les multiples possibilités des bibliothèques de collections. En PHP en peut aussi penser à utiliser un typage fort par exemple.

### L'architecture du code

Je connais peu de projets ou l'architecture du code est correctement documentée, bien qu'à ma grande surprise c'est un sujet qui revient souvent dans les rétrospectives. 
Le code évoluant fréquemment c'est l'occasion ici de partager sur les modèles (pattern) utilisés.

## Le nommage

Le nommage est aussi une phase importante de l'écriture d'un logiciel, une fois un nom choisi il devient de plus en plus difficile à changer avec le temps.
Même si avec les outils actuels (qui ne sont pas toujours utilisés par les développeurs) on peut facilement renommer des variables ou des méthodes / fonctions.
Il n'en reste pas moins, qu'une fois les noms choisis, ils passent aussi dans le vocabulaire courant et la cela devient de plus en plus difficile de renommer ce qui est dans le cerveau d'un être humain.
On peut se poser légitimement la question, la revue de code est-ce le bon moment pour discuter du nommage, et n'est-il pas déjà trop tard. Ne vaut-il pas mieux en discuter lors de séances de conception, de pair programming, de spécifications, de définition de user stories ?
En tout cas, la revue de code est le dernier moment ou peut éviter de laisser partir un nommage pas très heureux.

## L'adéquation aux spécifications

C'est le deuxième oeil sur ce qui a été développé et au delà du passage des tests unitaires et des tests fonctionnels (ce qui doit aussi être obligatoire), c'est l'occasion d'apporter un deuxième angle sur ce qui a été réalisé et vérifier que cela correspond bien a ce qui a été demandé.

## En résumé

La revue de code, permet certe d'améliorer la qualité du code produit en partageant les bonnes pratiques et en les propageant vers tout les membres de l'équipe. C'est aussi un moment ou l'on pourra vérifier la pertinence de l'implémentation et aussi l'exhaustivité de la réalisation.
Mais attention de ne pas tomber dans l’excès. Un développeur sachant que son code et son implémentation vont être revus peut être tenté de ne pas porter toute l'attention qu'il aurait du avoir si son code partait directement en production.

## Références

- [The Standard of Code Review](https://google.github.io/eng-practices/review/reviewer/standard.html)
- [What to look for in a code review](https://google.github.io/eng-practices/review/reviewer/looking-for.html#complexity)
- [Apprendre à apprendre avec le Lean, un passage obligé](https://leanagilite.com/2021/12/24/apprendre-a-apprendre-avec-le-lean-un-passage-oblige/)
