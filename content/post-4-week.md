+++
title = "Quatrième Semaine"
date = 2023-11-29
draft = false

[taxonomies]
categories = ["Ecrire"]
tags = ["Ecrire", "Blog"]
[extra]
keywords = "Ecrire, Weekly"
toc = true
series = "Ecrire"
image="images/post-4-week.jpg"
+++
{{ img(src="/images/post-4-week.jpg" alt="Zénitude des plantes") }}
*Zénitude des plantes*


Quatrième semaine, au moins je n'ai pas trop à réfléchir (pour l'instant) à trouver un titre. Je pourrais aussi appeler ce billet *rétrospective de la semaine*, car c'est un peu ce que je fais en général.

Peu d'écriture cette semaine, une paire d'exercice seulement d'écriture à la volée pendant 15 mn avec un rythme de 500 mots par quart d'heure. J'ai aussi noté vous avez dû le remarquer, les heures sur ces billets hebdomadaires, cela me permet d'avoir une vision du temps que prends la rédaction d'un article pour le blog. 

En résumé, 15 minutes pour l'écriture des mots et environ trois quart d'heure pour le reste : édition, correction, choisir une photo, et remanier un peu (mais pas trop le texte). Oui j'aime bien les photos en entêtes de billets, et j'ai modifié le thème du site en conséquence, pour que la photo d'entête s'affiche dans la liste des billets et correctement aussi lors de la lecture, que ce soit sur grand écran ou sur un mobile. C'est d'ailleurs sur un mobile que je consulte les sites la plupart du temps. 
J'ai aussi modifié le thème pour que lorsque l'on cite un article du blog la photo aussi apparaisse, en modifiant les balises qui vont bien (*og:image* par exemple).

Peu de dessin aussi, un croquis rapide lors de ma visite aux Bocaux d'Anna que j'ai posté sur mon compte [Pixelfed](https://pixelfed.fr/i/web/post/634017735407994559).


J'ai continué mon projet en [Scala](https://www.scala-lang.org/) utilisant [Play Framework](https://www.playframework.com/), et à noter cette semaine un séance de **TDD** en *"presque"* pair programming avec un de mes collègues à distance. Je dis *"presque"* pair programming car en réalité, c'est moi qui tenais le clavier, même si la partie a été assez interactive. 

Nous avons réussi à respecter le cycle **rouge vert rouge vert**, écrire le test, écrire le code, faire passer le test, remanier le code, en écrivant une nouvelle méthode d'un controlleur. Tout y est passé y compris le traitement d'erreur, ce que nous avions laissé un peu tomber jusque là. Le **TDD** est vraiment un bon moyen de réfléchir sur le code, de simplifier et aussi de généraliser. Il n'es pas facile par contre de n'écrire que le minimum, aussi bien au niveau du test qu'au niveau du code.

Je me suis aussi un peu remis à [Docker](https://www.docker.com/) à travers le projet [Lizmap](https://www.lizmap.com/) que je fais actuellement car, j'ai dû déployer une image *"custom"* pour le *"POC"* de traçage que nous sommes en train de faire. Dieu que j'aime le PHP !

Dans mes lectures, un article sur les estimations et la maintenance m'a interpellé et m'a fait pas mal réflèchir. Je n'ai malheureusement pas noté la référence. En essence, l'article mentionne que l'on oublie souvent lors des estimations de travail autour d'un projet de parler de la maintenance. Or, cette maintenance est obligatoire. L'auteur inclu aussi dans cette maintenance, la mise à jour des plate-formes utilisées, aussi bien les plate-formes de déploiement que les plate-formes de développement. Il estime à environ 10% le cout de celle-ci (en général moi, je donne plutôt 15%). En corollaire, si l'on oublie cette partie, il faut ensuite se battre pour faire rentrer ce temps dans les itérations, se battre avec le PO, pour que les Sprints / Kanban incluent cette maintenance.

Je me suis amusé à faire un petit calcul sur un coin de table. Considérant qu'une personne travaille sur un projet pendant un an, donc 200 jours, l'année suivante, il aura à sa disposition 180 jours : 200 - 10%, l'année suivante 162 jours ... etc. Cela ne vous rappelle rien ? Avez-vous l'expérience de projets qui ralentissent ? C'est pour cela qu'il est important de travailler sur l'aspect maintenance, et comme j'ai coutume de le dire, *"Une ligne de code en production est égale à un euro"*

Je m'arrête là. Moi qui pensais ce matin que je n'aurai pas le courage d'écrire un article long, je me suis laissé aller. L'écriture appelle l'écriture 

Je vous souhaite une très bonne semaine et un mercredi encore meilleur.

*21 mn de rédaction, 30 mn d'édition*