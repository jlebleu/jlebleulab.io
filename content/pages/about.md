+++
title = "About"
path = "about"
template = "pages.html"
draft = false
+++
This site is Jean-Yves LEBLEU personal website. Ce site est le site personnel de Jean-Yves LEBLEU.

Je parle de beaucoup de sujets, essentiellements autour du développement logiciel, de l'organisation des équipes du dessin aussi.

Tout les textes et images de ce site sont sous licenses [Creative Commons](https://creativecommons.org/licenses/by/4.0/) sauf mention du contraire.

Zola theme used is Abridge by [Jake G (jieiku)](https://github.com/Jieiku) and largely customized and modified so suite my needs.

Abridge is licensed under the [MIT license](https://opensource.org/licenses/MIT).


