
+++
title = "Ne Jamais dire Jamais"
date = 2023-12-15
draft = false
description="Une alternative à mockito never dans les tests avec scalatest"

[taxonomies]
categories = ["Dev"]
tags = ["dev", "mockito", "tdd", "mock", "scalatest", "scala"]
[extra]
keywords = "Mock, TDD"
toc = true
image="images/post-scala-never.jpg"
+++
{{ img(src="/images/post-scala-never.jpg" alt="Des tests, vous en reprendrez bien un bocal ?") }}
*Des tests, vous en reprendrez bien un bocal ?*


Lorsque l'on teste une méthode qui appelle un service en utilisant un mock, il est facile de vérifier qu'on appelle la bonne méthode avec les bons paramètres en utilisant la méthode verify de [mockito](https://site.mockito.org/).\
Mais qu'en est-il lorsque l'on insère une condition qui implique que l'on ne doit pas appeler la méthode. La tentation est grande d'utiliser un verify avec le mode de vérification never.

Exemple avec [scalatest](https://www.scalatest.org/) :

*Le code à tester*
```scala
def addinfo(user: User): = {
	user.info foreach(i => infoManager.addInfo(user.name, i)) 
}
```

Je vérifie ci dessous que je ne vais pas appeler la méthode addInfo d'infomanager

```scala

 "do not add information for a new user without info" in new Fixture {
    val service = ServiceFactory.create()
    val user = UserFactory.create(service.name).copy(info = None)

	userManager.addInfo(user, service)

	verify(infoManager, never()).addInfo(user.name, user.info)

    }    
```

Je me suis toujours posé la question de la pertinence de ce test, car imaginons que je transforme le code de la façon suivante.

```scala
def addinfo(user: User): = {
	infoManager.addInfo(user.name, "What ?") 
}
```

Lorsqu'on lance le teste précédent, le test passe, si user.info bien sûr n'est pas égal à "What ?".\
Lors de l'écriture du test, et si on le réalise en TDD, il y a peu de chance de tomber dans ce piège. Mais tout au long du projet, lorsqu'on va remanier le code, il y a des chances que l'on arrive à un cas similaire. Et dans ce cas le test en question ne teste plus rien.
C'est toujours périlleux de tester qu'on ne fait jamais quelque chose. Heureusement il y a `verifyNoInteractions` qui permet de vérifier qu'on a jamais d'interaction avec l'objet.

*Le même test avec `verifyNoInteractions`*
```scala

 "do not add information for a new user without info" in new Fixture {
    val service = ServiceFactory.create()
    val user = UserFactory.create(service.name).copy(info = None)

	userManager.addInfo(user, service)

	verifyNoInteractions(infoManager)

    }    
```

On peut aussi utiliser  `verifyNoMoreInteractions` lorsque l'on a dans le code d'autres interactions avec l'objet.

En conclusion pour ma part j'éviterais le `never` qui peut être délicat lors de refactoring au profit du `verifyNoInteractions` en n'oubliant pas l'[avertissement de la documentation](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html#verifyNoMoreInteractions(java.lang.Object...)) de ne pas systématiser l'utilisation de la méthode `verifyNoMoreInteractions`.


