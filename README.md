
# Personnal site using [getzola](https://www.getzola.org/)

## Run locally

```bash
docker run --rm -it -u "$(id -u):$(id -g)" -v $PWD:/app --workdir /app ghcr.io/getzola/zola:v0.17.2 build
```
```bash
docker run --rm -u "$(id -u):$(id -g)" -v $PWD:/app --workdir /app -p 8080:8080 -p 1024:1024 ghcr.io/getzola/zola:v0.17.2 serve --interface 0.0.0.0 --port 8080 --base-url localhost
```


## Update Abridge Theme

Remove content (except posts)
Remove template
Save config.toml

```bash
git submodule update --remote
```

Follow doc

```bash
 touch templates/.gitkeep
 rsync themes/abridge/config.toml config.toml
 rsync themes/abridge/content/_index.md content/
 rsync themes/abridge/COPY-TO-ROOT-SASS/* sass/
 rsync themes/abridge/netlify.toml netlify.toml
 rsync themes/abridge/package_abridge.js package_abridge.js
 rsync themes/abridge/package.json package.json
 rsync -r themes/abridge/content .
```

Update config.toml and saas/abridge.css (social icons)
